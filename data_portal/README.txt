data_portal README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/pserve development.ini

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The following instructions might be more helpful for newbees here like me.
Step 1. cd the directory that contains this README.txt file, which is YOUR_DATAPORTAL_REPOSITORY_PATH/data-portal/data_portal
Step 2. install dependencies in your virtual environment. 
	If you have not set up your virtual environment:
		(1). cd ~
		(2). mkdir yourenv
	$VENV actually refers to the path of yourenv path. Use '~/yourenv/bin/pip install -e .' to install all dependencies in your environment.
Step 3.Run the application! use '~/yourenv/bin/pserve developement.ini' to run it.
	If it said 'ImportError' which indicate some packages are missed, 'cd ~/yourenv/bin/' , then use 'pip install MissedPackageName' to install it.
		