import sys
sys.path.insert(0, '../data_portal')
import urllib.request
import json

# NOTE : For more info for using Urllib to get and post request check: https://docs.python.org/2/library/urllib.html
def test_get_dataset_api():

    	# 1. The API service will be continuously running through __init__.py file
    	# 2. This test sends the Get Request via urllib to the service to get the response from the API
    	# 3. This will fetch the datasets metadata info from the DB(metadata_dataset table)
    	# 4. This test compares the response should have the dataset_id in response

        dataset_id="10"
        api_url= "http://dev-dp-pyramid.stemformatics.org:8090/api/datasets/"+dataset_id
        get_api_response = urllib.request.urlopen(api_url)
        dataset_info = get_api_response.read().decode('UTF-8')

        assert dataset_id in dataset_info


def test_post_dataset_api():

        # 1. The API service will be continuously running through __init__.py file
        # 2. This test sends the Post request to the service and get respond from the API
        # 3. This will update the datasets metadata info into the DB(metadata_dataset table)
        # 4. Inputs to Curl Post Command:
        #        is_migrate : For migration purpose it should be True
        #        dp_base_url: Base URL of NeCTAR Swift storage e.g:
        #        dp_ensemble_version: It contains the information of current Version e.g: v67, v86 etc.
        #        dp_probes_file_location: probe file location on swift storage
        #        dp_yugene_file_location:  yugene file location on swift storage
        #        dp_cls_file_location: cls file location on swift storage
        #        dp_gct_file_location: gct file location on swift storage
        #        dp_rohart_file_location: rohart file location on swift storage
        # 5. This test compares the response should have the dataset_id and keyword "migrated" in response


        dataset_id = "1000"
        message_from_post_api_after_migration= "migrated"
        api_url= "http://dev-dp-pyramid.stemformatics.org:8090/api/datasets/"+dataset_id



        is_migrate = "True"
        dp_base_url = "https://swift.rc.nectar.org.au:8888/v1/AUTH_75bb46c0f1834c9cb0ab2af/data_portal_"+dataset_id +"/"
        dp_ensembl_version = "v69"
        dp_probes_file_location = "/1000.probes"
        dp_yugene_file_location ="/dataset1000.cumulative.txt"
        #dp_cls_file_location = "/.cls"
        dp_gct_file_location = "/dataset1000.gct"
        dp_rohart_file_location = "/dataset1000.rohart.MSC.txt"


        post_data = {'is_migrate' : is_migrate,
          'dp_base_url' : dp_base_url,
          'dp_ensembl_version' : dp_ensembl_version,
          'dp_probes_file_location' : dp_probes_file_location,
          'dp_yugene_file_location' : dp_yugene_file_location,
          #'dp_cls_file_location' : dp_cls_file_location,
          'dp_gct_file_location' : dp_gct_file_location,
          'dp_rohart_file_location' : dp_rohart_file_location
         }


        json_postdata= json.dumps(post_data).encode('UTF-8')

        req = urllib.request.Request(api_url, data=json_postdata,
                             headers={'content-type': 'application/json'})

        post_api_response = urllib.request.urlopen(req)

        message_from_post = post_api_response.read().decode('UTF-8')
        #
        # print(message_from_post)

        assert dataset_id,message_from_post_api_after_migration in message_from_post
