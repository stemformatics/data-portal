<%inherit file="../base.mako"/>
<!-- Add js files here -->

<%block name="partial_content">
    <%include file="./search_bar.mako"/>

    % if criteria:
        <%include file="./search_result_panel.mako"/>
    % endif
</%block>
