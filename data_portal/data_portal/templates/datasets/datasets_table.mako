<div class="table-panel">
    % if len(datasets) == 0:
    <h3>No Results.</h3>
    % else:
    <!-- Show Export selected dataset metadata button here. -->
    <div class="export-btn-group">
        <button class="datasets export-btn" data-href=${web_info['web_path'] + "/datasets/export?type=datasets"}>
            Export selected dataset metadata
        </button>
        <button class="samples export-btn" data-href=${web_info['web_path'] + "/datasets/export?type=samples"}>
            Export selected sample metadata
        </button>
        <button class="script export-btn" data-href=${web_info['web_path'] + "/datasets/export?type=download_script"}>
            Export download script for selected datasets
        </button>
    </div>
    <h5>${len(datasets)} datasets found.</h5>
    <div class="search-results">
        <table class="result-table table table-bordered">
            <thead>
                <tr>
                    <th class="col-md-1 toggle-select-all">Click to toggle</th>
                    <th class="col-md-1">ID</th>
                    <th class="col-md-2">Handle</th>
                    <th class="col-md-3">Title</th>
                    <th class="col-md-2">Species</th>
                    <th class="col-md-1">Samples/Total</th>
                    <th class="col-md-2">Actions</th>
                </tr>
            </thead>
            <tbody>
            <% dataset_ids = datasets.keys() %>
            %for ds_id in dataset_ids:
            <tr class="dataset">
                <% ds = datasets[ds_id]%>
                <td><input type="checkbox" checked value=${ds_id}></td>
                <td>${ds_id}</td>
                <td>${ds['handle']} <br>[${ds['data_type']}]</td>
                <td>${ds['title']}</td>
                <td>${ds['species']}</td>
                <td>${len(ds['matched_samples'])}/${ds['total_samples_count']}</td>
                <td>
                    <div class="dropdown">
                       <button type="button" class="actions-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Actions...
                        <span class="caret"></span>
                       </button>
                      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a  target="_blank" href="${web_info['stemformatics_web_path']}/datasets/view/${ds_id}">Link back to Stemformatics</a></li>
                        <li><a href="./view/${ds_id}">View Datasets Summary</a></li>
                        <li><a href="#" id="toggle-sample-view">Toggle Samples View</a></li>
                        %if set(['dp_base_url', 'dp_ensembl_version', 'dp_gct_file_location']).issubset(ds):
                            <li>
                                <a target="_blank" href="${ds['dp_base_url'] + ds['dp_ensembl_version'] + '/' +ds['dp_gct_file_location']}">Download Normalised</a>
                            </li>
                        %endif
                        %if ds['show_yugene'] and set(['dp_base_url', 'dp_ensembl_version', 'dp_yugene_file_location']).issubset(ds):
                            <li>
                                <a target="_blank" href="${ds['dp_base_url'] + ds['dp_ensembl_version'] + '/' +ds['dp_yugene_file_location']}">Download Yugene</a>
                            </li>
                        %endif
                      </ul>
                    </div>
                </td>
            </tr>
            %endfor
            </tbody>
        </table>
    </div>
    % endif
</div>
