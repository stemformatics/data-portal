from models.base_model import BaseModel as base_model

import json, re, operator, collections
import sys
# import numpy as fnp

from collections import OrderedDict

sys.path.insert(0, '../data_portal')

from config import *

class DatasetModel:

    # Data in: Dictionary data from the database, e.g. [['7021', 'chip_id_1'], ['6341', 'chip_id_2'], ['5670', 'chip_id_3']]
    # Data out: With set index as 0, flatten array structure will be, e.g. ['7021', '6341', '5670']
    #           With set index as 1, flatten array structure will be, e.g. ['chip_id_1', 'chip_id_2', 'chip_id_3']
    @staticmethod
    def _get_ids(array, index):
        flatten_array = []
        for element in array:
            flatten_array.append(element[index])
        return flatten_array

    @staticmethod
    def _get_species(db_id):
        species_dic = {'46': 'Mus musculus', '56': 'Homo sapiens'}
        return species_dic[str(db_id)]

    # Combine all dataset_metadata into a dict
    @staticmethod
    def _encodeData (ds_id, ds_mt_result):
        returnList = {
                ds_id: {
                            "project": ds_mt_result.get("project", {}),
                            "top_miRNA": ds_mt_result.get("top_miRNA", {}),
                            "accession_id": ds_mt_result.get("Accession", "N/A"),
                            "geo_accession_id": ds_mt_result.get("GEO Accession", "N/A"),
                            "ae_accession_id": ds_mt_result.get("AE Accession", "N/A"),
                            "sra_accession_id": ds_mt_result.get("SRA Accession", "N/A"),
                            "pxd_accession_id": ds_mt_result.get("PXD Accession", "N/A"),
                            "ena_accession_id": ds_mt_result.get("ENA Accession", "N/A"),
                            "gene_pattern_analysis_access": ds_mt_result.get("genePatternAnalysisAccess", "Allow"),
                            "cells_samples_assayed": ds_mt_result.get("cellsSamplesAssayed", "N/A"),
                            "probeName": ds_mt_result.get("probeName", ""),
                            "sample_type_order": ds_mt_result.get("sampleTypeDisplayOrder", "N/A"),
                            "title": ds_mt_result.get("Title", "N/A"),
                            "publication_title": ds_mt_result.get("Publication Title", "N/A"),
                            "name": ds_mt_result.get("Contact Name", "N/A"),
                            "email": ds_mt_result.get("Contact Email", "N/A"),
                            "affiliation": ds_mt_result.get("Affiliation", "N/A"),
                            "authors" : ds_mt_result.get("Authors", "N/A"),
                            "probes detected" : ds_mt_result.get("probesDetected", "N/A"),
                            "breakDown" : ds_mt_result.get("breakDown", "N/A"),
                            "public_release_date" : ds_mt_result.get("Release Date", "N/A"),
                            "pub_med_id": ds_mt_result.get("PubMed ID", "N/A"),
                            "description": ds_mt_result.get("Description", "N/A"),
                            "platform": ds_mt_result.get("Platform", "N/A"),
                            "RohartMSCAccess": ds_mt_result.get("RohartMSCAccess", "N/A"),
                            "probes": ds_mt_result.get("probeCount", "N/A"),
                            "organism": ds_mt_result.get("Organism", "N/A"),
                            "top_diff_exp_genes": ds_mt_result.get("topDifferentiallyExpressedGenes", "N/A"),
                            "replicates": ds_mt_result.get("minReplicates", "N/A") + '/' +  ds_mt_result.get("maxReplicates", "N/A"),
                            "showReportOnDatasetSummaryPage": ds_mt_result.get("showReportOnDatasetSummaryPage", []),
                            "ShowExternalLinksOnDatasetSummaryPage": ds_mt_result.get("ShowExternalLinksOnDatasetSummaryPage", []),
                            "has_data": ds_mt_result.get("has_data", 'yes'),
                            "datasetStatus": ds_mt_result.get("datasetStatus", ''),
                            "dp_base_url": ds_mt_result.get("dp_base_url", ''),
                            "dp_ensembl_version": ds_mt_result.get("dp_ensembl_version", ''),
                            "dp_probes_file_location": ds_mt_result.get("dp_probes_file_location", ''),
                            "dp_yugene_file_location": ds_mt_result.get("dp_yugene_file_location", ''),
                            "dp_gct_file_location": ds_mt_result.get("dp_gct_file_location", ''),
                            "dp_rohart_file_location": ds_mt_result.get("dp_rohart_file_location", ''),
                        }
                    }
        return returnList

    #Data in: Criteria from the browser
    #Data out: All datasets details matching with full text search criteria
    @staticmethod
    def simple_search(criteria, selected_ds_ids=None):

        if criteria:
            # https://stackoverflow.com/questions/9519734/python-regex-to-find-a-string-in-double-quotes-within-a-string
            search_on_double_quotes_list = re.findall(r'\"(.+?)\"',criteria)
            for item in search_on_double_quotes_list:
                criteria.replace(item, "")

            criterion_list = search_on_double_quotes_list
            split_on_space_array  = criteria.split()
            for term in split_on_space_array:
                criterion_list.append(term.strip())

        else:
            criterion_list = []

        dataset_id_array = []
        matched_samples_by_ds_id = {}
        datasets_detail = {}
        # Need add ds_ids here rahter than just count to record matched ds_ids under each subcategory
        '''
            The data structire of datasets_detail dict (Number shows how many results are under the current category ):
                categories_detail = {
                    'Species': {
                            'Homo sapiens': ['1000', '2200'],
                            'Mus Musculus': ['1234']
                    },
                    'Tissue Types': {
                            'Tissue Type 1':  ['1000', '2200', '1234']
                    },
                    'Cell Types': {
                            'Cell Type 1': ['1000', '2200', '1234']
                    }
                }
        '''
        samples_detail = {}
        '''
            Structure of samples_detail dictionary:
                {
                    'chip_type_1': {
                            'chip_id_1': {
                                    'ds_id_1': {
                                        'sample_id': '',
                                        'sample_type': ''
                                    }
                                },
                            'chip_id_2': {
                                    'ds_id_2': {
                                        'sample_id': '',
                                        'sample_type': ''
                                    }
                                }
                        }
                    }

        '''
        list_of_valid_categories = ['Species', 'Tissue Types', 'Cell Types']
        species = list_of_valid_categories[0]
        tissue_types = list_of_valid_categories[1]
        cell_types = list_of_valid_categories[2]

        categories_detail = {species:{}, tissue_types:{}, cell_types:{}}
        chip_ids_of_matched_samples = []

        for criterion in criterion_list:
            is_searching_ds_id = criterion.strip().startswith('D#')

            if is_searching_ds_id:
                # Extract dataset id from criterion, e.g. criterion = 'D#1000', then ds_id = '1000'

                ds_id = criterion.strip()[2:]
                if (len(ds_id) != 0) and (ds_id.isdigit()):
                    dataset_metadata_sql ='select ds_id from dataset_metadata where ds_id=%s group by ds_id'
                    biosample_metadata_sql = 'select ds_id, max(chip_type), chip_id from biosamples_metadata where ds_id=%s '
                    biosample_metadata_sql += ' group by ds_id, chip_id order by ds_id, chip_id '
                    search_pattern = [ds_id]
                else:
                    break
            else:
                dataset_metadata_sql ='select ds_id from dataset_metadata where ds_value ilike %s group by ds_id'
                biosample_metadata_sql = 'select ds_id, max(chip_type), chip_id from biosamples_metadata where md_value ilike %s '
                biosample_metadata_sql += 'group by ds_id, chip_id order by ds_id, chip_id '
                search_pattern = ["%"+ criterion.strip() +"%"]

            dataset_metadata_ds_ids = base_model._get_psycopg2_sql(dataset_metadata_sql, search_pattern)
            biosamples = base_model._get_psycopg2_sql(biosample_metadata_sql, search_pattern)

            ids = DatasetModel._get_ids(dataset_metadata_ds_ids, 0)
            dataset_id_array.extend(ids)

            # Record all matched sample of current dataset
            for biosample in biosamples:
                ds_id = str(biosample[0])
                chip_type = biosample[1]
                chip_id = biosample[2]
                if selected_ds_ids and (ds_id not in selected_ds_ids):
                        continue
                if ds_id not in matched_samples_by_ds_id:
                    matched_samples_by_ds_id[ds_id] = []
                if biosample not in matched_samples_by_ds_id[ds_id]:
                    matched_samples_by_ds_id[ds_id].append(biosample)
                    # construct samples_details: record all chip ids of matched samples in chip_ids_of_matched_samples variable
                    if chip_type not in samples_detail:
                        samples_detail[chip_type] = {}
                    if chip_id not in samples_detail[chip_type]:
                        samples_detail[chip_type][chip_id] = {}
                    if ds_id not in samples_detail[chip_type][chip_id]:
                        samples_detail[chip_type][chip_id][ds_id] = {}
                    chip_ids_of_matched_samples.append(chip_id)

            ids = DatasetModel._get_ids(biosamples, 0)
            dataset_id_array.extend(ids)

        tuple_chip_ids = [tuple(sorted(chip_ids_of_matched_samples))]
        if selected_ds_ids:
            unique_dataset_ids = sorted(list(map(int, selected_ds_ids)))
        else:
            unique_dataset_ids = sorted(set(dataset_id_array))

        '''
            Form dataset dictionaries (datasets_detail) showed in the search result table
                       E.g.  {
                                '1000': {
                                              'handle':'***',
                                              'title': '***',
                                              'species':'***',
                                              'matched_samples': [],
                                              'total_samples_count': **,
                                              'show_yugene': False/True,
                                              'dp_base_url': '***',
                                              'dp_ensembl_version': '***',
                                              'dp_yugene_file_location': '***',
                                              'dp_gct_file_location': '***'
                                            }
                            }

        '''

        if len(unique_dataset_ids) != 0:
            # Need to update SQL here to return Tissue Type/Cell Type/Species as well
            tuple_ds_ids = [tuple(unique_dataset_ids)]

            datasets_sql = 'select d.id, d.handle, d.number_of_samples, d.db_id, dt.name, d.show_yugene from datasets as d left join '
            datasets_sql += 'data_types as dt on d.data_type_id = dt.id where d.id IN %s order by d.id ASC'
            datasets = base_model._get_psycopg2_sql(datasets_sql, tuple_ds_ids)

            dataset_metadata_sql = "select ds_id, ds_name, ds_value from dataset_metadata where ds_id IN %s AND ds_name IN "
            dataset_metadata_sql += "('Title', 'dp_base_url', 'dp_ensembl_version', 'dp_yugene_file_location', 'dp_gct_file_location') order by ds_id ASC"
            datasets_metadata = base_model._get_psycopg2_sql(dataset_metadata_sql, tuple_ds_ids)

            if chip_ids_of_matched_samples:
            # Get all biosamples information of matched samples
                biosamples_sql = "select ds_id, md_value, md_name, max(chip_type), chip_id from biosamples_metadata "
                biosamples_sql += "where chip_id IN %s AND (md_name='Tissue/organism part' OR md_name='Cell Type' OR md_name='Replicate Group ID' OR md_name='Sample Type') "
                biosamples_sql += "group by ds_id, md_value, md_name, chip_id"
                biosamples = base_model._get_psycopg2_sql(biosamples_sql, tuple_chip_ids)
            else:
                biosamples = []

            for biosample in biosamples:
                ds_id = biosample[0]
                ds_id_str = str(ds_id)
                md_value = biosample[1]
                md_name = biosample[2]
                chip_type = biosample[3]
                chip_id= biosample[4]
                if ds_id in unique_dataset_ids:
                    # Count the search result under each tissue type and each cell type
                    # Also, should change here to append ds_ids into datasets_detail, then we can know what datasets should be
                    # displayed after clicking a subcategory in the front end
                    if md_name == 'Tissue/organism part':
                        if md_value not in categories_detail[tissue_types]:
                            categories_detail[tissue_types][md_value] = []
                        if ds_id_str not in categories_detail[tissue_types][md_value]:
                            categories_detail[tissue_types][md_value].append(ds_id_str)
                    if md_name == 'Cell Type':
                        if md_value not in categories_detail[cell_types]:
                            categories_detail[cell_types][md_value] = []
                        if ds_id_str not in categories_detail[cell_types][md_value]:
                            categories_detail[cell_types][md_value].append(ds_id_str)
                    # Record matched sample details
                    if md_name == 'Replicate Group ID' and (ds_id_str in samples_detail[chip_type][chip_id]):
                        samples_detail[chip_type][chip_id][ds_id_str]['sample_id'] = md_value
                    if md_name == 'Sample Type' and (ds_id_str in samples_detail[chip_type][chip_id]):
                        samples_detail[chip_type][chip_id][ds_id_str]['sample_type'] = md_value

            for index in range(len(datasets)):
                ds_id = str(datasets[index][0])
                datasets_detail[ds_id] = {}

                datasets_detail[ds_id]['handle'] = datasets[index][1]
                datasets_detail[ds_id]['total_samples_count'] = datasets[index][2]
                datasets_detail[ds_id]['data_type'] = datasets[index][4]
                datasets_detail[ds_id]['show_yugene'] = datasets[index][5]

                db_id = datasets[index][3]

                species_name = DatasetModel._get_species(db_id)
                datasets_detail[ds_id]['species'] = species_name

                if ds_id in matched_samples_by_ds_id:
                    datasets_detail[ds_id]['matched_samples'] = matched_samples_by_ds_id[ds_id]
                else:
                    datasets_detail[ds_id]['matched_samples'] = []

                # Count the search result under every species
                if species_name not in categories_detail['Species']:
                    categories_detail['Species'][species_name] = []

                if  ds_id not in categories_detail['Species'][species_name]:
                    categories_detail['Species'][species_name].append(ds_id)

            for ds in datasets_metadata:
                ds_id = str(ds[0])
                ds_name = ds[1].lower()
                ds_value = ds[2]
                datasets_detail[ds_id][ds_name] = ds_value

            # descend cell_types and tissue_types by countss
            # Using a OrderedDict to keep the order.
            # Solution Ref: https://stackoverflow.com/questions/9460406/sort-dict-by-value-and-return-dict-not-list-of-tuples
            sorted_tissue_types = OrderedDict(sorted(categories_detail[tissue_types].items(), key=operator.itemgetter(1),reverse=True))
            sorted_cell_types = OrderedDict(sorted(categories_detail[cell_types].items(), key=operator.itemgetter(1),reverse=True))
            categories_detail[tissue_types] = sorted_tissue_types
            categories_detail[cell_types] = sorted_cell_types

        return {
                    'datasets_detail': datasets_detail,
                    'categories_detail': categories_detail,
                    'samples_detail': samples_detail
                }

    def get_dataset_metadata(ds_id):
        # 1. search DB to get all needed dataset metadata for the summary page via ds_id
        # 2. call _encodeData to form data and then return

        int_ds_id = [int(ds_id)]
        ds_md_sql = 'select * from dataset_metadata where ds_id = %s'
        ds_md_results = base_model._get_psycopg2_sql(ds_md_sql, int_ds_id)

        temp_result_dict = {}
        for row in ds_md_results:
             ds_name =  row['ds_name']
             ds_value =  row['ds_value']
             temp_result_dict[ds_name] = ds_value
        returnDict = DatasetModel._encodeData(ds_id, temp_result_dict)

        # Add dataset information into returnList
        ds_sql = 'select handle, number_of_samples, show_yugene from datasets where id = %s';
        ds_results = base_model._get_psycopg2_sql(ds_sql, int_ds_id)
        ds_detail = ds_results[0]
        returnDict[ds_id]['handle'] = ds_detail[0]
        returnDict[ds_id]['number_of_samples'] = ds_detail[1]
        returnDict[ds_id]['show_yugene'] = ds_detail[2]

        return returnDict

    @staticmethod
    def export_downloadable_data (export_type, search_criteria, ds_ids):
        '''
             1. Invoke simple_search method to get all searched datasets details
             2. if export_type is "dataset"
                row_list in dataset_metadata file should include ['ds_id','handle','title','organism','samples found','all_samples']
             3. Form a string including all selected datasets details to return
        '''
        selected_ds_ids = []
        if len(ds_ids) != 0:

            selected_ds_ids = ds_ids.split(',')

        result = DatasetModel.simple_search(search_criteria);
        datasets = result['datasets_detail']
        samples = result['samples_detail']
        if export_type == 'datasets':
            row_list = ['ds_id','handle','title','organism','samples found','all_samples']
            return_data = "\t".join(row_list) + "\n"
            for ds_id in selected_ds_ids:
                ds_id = ds_id.strip()

                row_list = []
                row_list.append(str(ds_id))
                row_list.append(datasets[ds_id]['handle'])
                row_list.append(datasets[ds_id]['title'])
                row_list.append(datasets[ds_id]['species'])
                row_list.append(str(len(datasets[ds_id]['matched_samples'])))
                row_list.append(str(datasets[ds_id]['total_samples_count']))

                row = "\t".join(row_list) + "\n"
                return_data  += row

        if export_type =='samples':

            row_list = ['species','ds_id','handle','chip_id','sample_id','sample_type']
            return_data = "\t".join(row_list) + "\n"
            for ds_id in selected_ds_ids:

                for sample in datasets[ds_id]['matched_samples']:
                    row_list = []
                    row_list.append(datasets[ds_id]['species'])
                    row_list.append(str(ds_id))
                    row_list.append(datasets[ds_id]['handle'])
                    chip_type = sample[1]
                    chip_id = sample[2]
                    sample_metadata = samples[chip_type][chip_id][ds_id]
                    row_list.append(chip_id)
                    row_list.append(sample_metadata['sample_id'])
                    row_list.append(sample_metadata['sample_type'])

                    row = "\t".join(row_list) + "\n"
                    return_data +=row

        if export_type =='download_script':
             return_data = DatasetModel.create_download_script_for_multiple_datasets(datasets,selected_ds_ids)

        return return_data

    @staticmethod
    def advanced_search(criteria):
        return 0

    @staticmethod
    def get_all_metadata_for_dataset_by_id(ds_id):

        try:
            ds_id= int(ds_id)

        except ValueError:
                return "dataset id should be integer"

        datasets_sql_query = 'select * from datasets where datasets.id=%s;'
        data=(ds_id,)
        dataset_from_ds_table = base_model._get_psycopg2_sql(datasets_sql_query,data)

        dataset_metadata_sql_query = 'select * from dataset_metadata where dataset_metadata.ds_id=%s;'
        data=(ds_id,)
        dataset_from_md_table = base_model._get_psycopg2_sql(dataset_metadata_sql_query,data)

        biosamples_metadata_sql_query = 'select * from biosamples_metadata where biosamples_metadata.ds_id=%s;'
        data=(ds_id,)
        dataset_from_bm_table = base_model._get_psycopg2_sql(biosamples_metadata_sql_query,data)

        # all the metadata datasets into a dictionary object
        final_data_structure_to_return ={}

        dataset_dict={"datasets" :[]}
        metadata_dict={"dataset_metadata":[]}
        biosamples_dict={"biosamples_metadata":[]}

        #For populating all datasets from datasets table into a dictionary
        for row in dataset_from_ds_table:
            d = {}
            d['id'] = row['id']
            d['lab'] = row['lab']
            d['dtg'] = str(row['dtg'])
            d['handle'] = row['handle']
            d['published'] = row['published']
            d['private'] = row['private']
            d['chip_type'] = row['chip_type']
            d['min_y_axis'] = row['min_y_axis']
            d['show_yugene'] = row['show_yugene']
            d['show_limited'] = row['show_limited']
            d['db_id'] = row['db_id']
            d['number_of_samples'] = row['number_of_samples']
            d['data_type_id'] = row['data_type_id']
            d['mapping_id'] = row['mapping_id']
            d['log_2'] = row['log_2']
            dataset_dict["datasets"].append(d)

         #For populating all metadata from dataset_metadata table into a dictionary
        for row in dataset_from_md_table:
            d = {}
            d['ds_id'] = row['ds_id']
            d['ds_name'] = row['ds_name']
            d['ds_value'] = row['ds_value']

            metadata_dict["dataset_metadata"].append(d)

        #For populating all metadata from dataset_metadata table into a dictionary
        for row in dataset_from_bm_table:
            d = {}
            d['ds_id']=row['ds_id']
            d['chip_type'] = row['chip_type']
            d['chip_id'] = row['chip_id']
            d['md_name'] = row['md_name']
            d['md_value']=row['md_value']

            biosamples_dict["biosamples_metadata"].append(d)

        final_data_structure_to_return = {ds_id : {'datasets': dataset_dict["datasets"],
                             'dataset_metadata':metadata_dict["dataset_metadata"], 'biosamples_metadata':biosamples_dict["biosamples_metadata"]}}


        return (final_data_structure_to_return)

    @staticmethod
    def set_metadata_for_migration(ds_id,list_to_insert):

        try:
            ds_id= int(ds_id)

        except ValueError:
                return "dataset id should be integer"

        check_successful_insert=False

        for keys, value in list_to_insert.items():
            if(keys != "is_migrate"):
                    datasets_sql_query = 'insert into dataset_metadata(ds_id,ds_name,ds_value) values (%s,%s,%s);'
                    data=(ds_id,keys,value,)
                    if(base_model._get_psycopg2_sql(datasets_sql_query, data, True)==True):
                        check_successful_insert=True
                    else:
                        check_successful_insert=False

        return check_successful_insert

    @staticmethod
    def breakdown_of_all_datasets():

        breakdown_datasets = []
        try:
            datasets_sql_query = 'select dt.name,count(*) from datasets as d left join data_types as dt on dt.id = d.data_type_id group by dt.name;'
            data = base_model._get_psycopg2_sql(datasets_sql_query)
            for breakdown_dataset in data:
                temp_data = {}
                temp_data['data_type'] = breakdown_dataset[0]
                temp_data['count'] = breakdown_dataset[1]
                breakdown_datasets.append(temp_data)
        except ValueError:
                return "error in Database connectivity"

        return breakdown_datasets

    @staticmethod
    def create_download_script_for_multiple_datasets(datasets,selected_ds_ids,file_types=['yugene','gct']):


        return_data = "#!/bin/bash\n"
        return_data += "####################################################################\n"
        return_data += "# This is a script to download from a Mac/Linux/Unix computer\n"
        return_data += "# You will need to have wget installed \n"
        return_data += "# The following commands assume that you are on a terminal in the same directory as the script\n"
        return_data += "# You will need to make this script executable by using the command below\n"
        return_data += "# chmod 744 multi_download_script.sh \n"
        return_data += "# You will need to run by using the command below \n"
        return_data += "# ./multi_download_script.sh \n"
        return_data += "####################################################################\n"

        int_ds_ids=[]


        for ds_id in selected_ds_ids:
            ds_id=int(ds_id)
            int_ds_ids.append(ds_id)


        tuple_ds_ids = [tuple(int_ds_ids)]

        #Calling the database model method to fetch the url

        datasets_sql_query = "select M.ds_id,M.ds_name,M.ds_value,D.handle from dataset_metadata M "
        datasets_sql_query += "Left join datasets D on D.id = M.ds_id where M.ds_name IN "
        datasets_sql_query += "('dp_base_url','dp_ensembl_version', 'dp_probes_file_location',"
        datasets_sql_query += "'dp_yugene_file_location','dp_rohart_file_location' ,'dp_gct_file_location')"
        datasets_sql_query += " and M.ds_id IN %s order by M.ds_id;"
        all_dp_metadata = base_model._get_psycopg2_sql(datasets_sql_query,tuple_ds_ids)


        dict_of_metadata = {}


        dict_of_metadata = {};
        for dp_metadata in all_dp_metadata:
            ds_id = str(dp_metadata[0])
            dp_key=dp_metadata[1]
            dp_value=dp_metadata[2]
            dp_handle=dp_metadata[3]

            if ds_id not in dict_of_metadata:
                dict_of_metadata[ds_id] = {}

            dict_of_metadata[ds_id][dp_key] = dp_value
            dict_of_metadata[ds_id]['dp_handle'] =dp_handle

        for key,value in dict_of_metadata.items():

            base_url=value['dp_base_url']
            ensemble_version=value['dp_ensembl_version']
            file_handle=value['dp_handle']
            yugene_file_location=value['dp_yugene_file_location']
            gct_file_location=value['dp_gct_file_location']

            handle_yugene_file =str(key) + "_" + file_handle + ".yugene.txt\n"
            handle_gct_file = str(key) + "_" + file_handle + ".gct\n"
            url_yugene_download = base_url+ensemble_version+"/"+yugene_file_location;
            url_gct_download = base_url+ensemble_version+"/"+gct_file_location;

            for temp_type in file_types:
                if temp_type == 'yugene':
                    yugene_file = "wget \""+url_yugene_download+"\" -O " + handle_yugene_file
                if temp_type == 'gct':
                    gct_file = "wget \""+url_gct_download+"\" -O " + handle_gct_file

            return_data +=yugene_file
            return_data +=gct_file

        return return_data
