import sys
from config import *
sys.path.insert(0, '../data_portal')

class BaseModel:

	@staticmethod
	def _get_psycopg2_sql(sql, data = None, commit = False):
		conn = psycopg2.connect(psycopg2_conn_string)
		cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)

		cursor.execute(sql, data)

		if commit:
			conn.commit()
			result= True
		else:
		 	result = cursor.fetchall()

		cursor.close()
		conn.close()

		return result
