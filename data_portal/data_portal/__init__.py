from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response

from controllers.datasets_controller import DatasetsController

def include_packages(config):
    config.include('pyramid_mako')
    config.include('pyramid_handlers')

    #This will include the package of Cornice with Pyramid App
    config.include('cornice')

def config_routings(config):
    # add a handler here
    config.add_handler('home', '/',
                   handler=DatasetsController, action='search')
    config.add_handler('tests', '/datasets/tests',
                   handler=DatasetsController, action='tests')
    config.add_handler('view', '/datasets/view/{ds_id}',
                   handler=DatasetsController, action='view')
    config.add_handler('datasets_search', '/datasets/search',
                   handler=DatasetsController, action='search')
    config.add_handler('export', '/datasets/export',
                   handler=DatasetsController, action='export')
    config.add_handler('filter_categories_selection', '/datasets/filter_categories_selection',
                   handler=DatasetsController, action='filter_categories_selection')

def config_server(config):
    app = config.make_wsgi_app()

    # For Sadia API work port 5001 is opened , for Huan its 5003, for beta its 5000
    server = make_server('0.0.0.0', 5000, app)

    server.serve_forever()


def config_static_view(config):
    config.add_static_view(name='js', path='data_portal_public/js')
    config.add_static_view(name='css', path='data_portal_public/css')
    config.add_static_view(name='images', path='data_portal_public/images')

if __name__ == '__main__':
    config = Configurator()

    config_static_view(config)
    include_packages(config)
    config_routings(config)

    #This is for API Cornice Service
    config.scan('controllers.api')
    config.scan()

    config_server(config)
