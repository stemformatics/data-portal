import psycopg2
import psycopg2.extras

# Web path
web_path = "https://portal.stemformatics.org"
stemformatics_web_path = 'https://www.stemformatics.org/'

# Site name
site_name =  'Stemformatics'

# version number
data_portal_version = 'v0.1'

# Add all base web information here
web_info = {
    'web_path': web_path,
    'stemformatics_web_path': stemformatics_web_path,
    'site_name': site_name,
    'data_portal_version': data_portal_version
}

# Database connection string
psycopg2_conn_string = "dbname=public_data user=portaladmin"
